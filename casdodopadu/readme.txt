in-source parameters:
A_c - hamaker constant
m - mass of nanoparticle
rlim - minimal distance between particles
epsSameE - allowed error for the difference between energy in time 0 and T

Simulation is ran by function "timego", which calculates the time to impact and impact velocity for 'N' initial distances between 'rlim' and 'R' and saves result into 'folder'.
Parameter 'one_particle_is_fixed' switches between cases where a) one particle is fixed and only the forces apply only on the second one, b) both particles are moving together.

