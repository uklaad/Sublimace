import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco
import os

#konstanty
A_c = 8.4e-29 #hamakerova konstanta
m = 1.56e-20  #hmotnost castice
#presnost zakona zachovani energie
epsSameE = 1e-6


A = A_c / m

#minimalni vzdalenost (mrizkova konstanta)
rlim = 1.417e-9


#fit polynomem pro odhad doby impactu
esta = 5.32e11
estb = -2.71e6
estc = 3.82e1
estd = 0




def ensure_dir(folder):
    #dir = os.path.dirname(__file__)
    file_path = os.getcwd()+'/'+folder+'/'
    print(file_path)
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)

#fitovani krivky polynomy pro odhad dobydo impactu
def fitP2(t,y, tim = 1e6):
  #ax**2 + bx
  
  x0 = [8.,0.,0.]
  
  r = np.array(t)*tim
  y = np.array(y)*tim
  
  def fit(x):
    yp = x[0]*np.power(r,2)+x[1]*r+x[2]
    #~ print(yp)
    d = yp-y
    #~ print(d)
    out = sum( np.power(d,2) )
    return out
    
  def jac(x):
    out=np.zeros_like(x)
    
    yp = x[0]*np.power(r,2)+x[1]*r+x[2]
    d = yp-y
    
    #~ print('jac')
    #~ print(d*np.power(r,2))
    #~ print(d*r)
    #~ print(d)
    
    out[0] = 2*sum( d*np.power(r,2) )
    out[1] = 2*sum( d*r )
    out[2] = 2*sum( d )
    
    return out
    
  
  res = sco.minimize(fit,x0, method='cg', tol=1e-8, jac = jac, options = {'maxiter':1000})
  #print(res)
  
  out = res.x
  out[0] *= tim
  out[2] /= tim
  return out
  
def fitP3(t,y, tim = 1e6):
  #ax**3 + bx**2 + cx + d
  
  x0 = [8.,0.,0.,0.]
  
  r = np.array(t)*tim
  y = np.array(y)*tim
  
  def fit(x):
    yp = x[0]*np.power(r,3)+x[1]*np.power(r,2)+x[2]*r + x[3]
    #~ print(yp)
    d = yp-y
    #~ print(d)
    out = sum( np.power(d,2) )
    return out
    
  def jac(x):
    out=np.zeros_like(x)
    
    yp = x[0]*np.power(r,3)+x[1]*np.power(r,2)+x[2]*r + x[3]
    d = yp-y
    
    #~ print('jac')
    #~ print(d*np.power(r,2))
    #~ print(d*r)
    #~ print(d)
    
    out[0] = 2*sum( d*np.power(r,3) )
    out[1] = 2*sum( d*np.power(r,2) )
    out[2] = 2*sum( d*r )
    out[3] = 2*sum( d )
    
    return out
    
  
  res = sco.minimize(fit,x0, method='cg', tol=1e-8, jac = jac, options = {'maxiter':1000})
  #print(res)
  
  out = res.x
  out[0] *= tim*tim
  out[1] *= tim
  out[3] /= tim
  return out


#model a resic

def EP(x, one_particle_is_fixed = True):
  """kineticka energie stavu x"""
  out = -A_c/x[0]
  
  out = out if one_particle_is_fixed else 2*out
  
  return out

def dfdt1c(x):
  """diferencial v x pro pripad pevne castice"""
  out = np.zeros(2)
  out[0] = x[1]
  out[1] = -A/np.power(x[0],2)
  #one is moving
  return out
  
def dfdt2c(x):
  """diferencial v x pro pripad obou volnych castic"""
  out = np.zeros(2)
  out[0] = x[1]
  out[1] = -2*A/np.power(x[0],2)
  #both moving
  return out

  
def RK4(x,dt, dfdt = dfdt1c):
  """rungekuttuv krok"""
  dh = dt/2
  
  k1 = dfdt(x)
  
  x1 = x + k1*dh
  k2 = dfdt(x1)
  
  x2 = x + k2*dh
  k3 = dfdt(x2)
  
  x3 = x + k3*dt
  k4 = dfdt(x3)
  
  dk = (k1+2*k2+2*k3+k4)
  out = x + dk*dt/6
  return out
  
 
 
#integruje a spocte cas dopadu
def ftime(R, N = 1000, show = False, lpow = 1, estT = 1., stable = True):
  """\tR - pocatecni vydalenost 
\tN pocet deleni
\tshow vykresli
\tstable - jestli vyzadujeme zachovani
  
  """
  print(estT)
  y = np.array([R,0])
  
  ys = [y]
  ts = [0]
  
  ttt = np.logspace(-1,lpow,N+1)*(1./(10**lpow - 1))*estT
  tt = ttt[-1::-1]
  dt = np.zeros(N)
  
  sign = 1.
  if tt[-1] > tt[0]:
    sign = -1.
  for i in range(N):
    dt[i] = tt[i]-tt[i+1]
  dt*= sign
  
  #plt.figure();plt.plot(dt);plt.title('dt');plt.show(0)
  
  ep0 = EP(y, one_particle_is_fixed)
  
  #iteruje dokud nenarazi
  count = 0
  while(y[0] > rlim):
    y = RK4(y,dt[count])
    ys.append(y)
    ts.append(ts[-1]+dt[count])
    
    if count < N-1:
      count += 1
  
  ys = np.array(ys)
  
  ep1 = EP(ys[-2], one_particle_is_fixed)
  ek1 = m*np.power(ys[-2,1],2)/2  
  
  if show:
    plt.figure(); plt.plot(ts,ys[:,0]); plt.show(0)
    plt.figure(); plt.plot(ts,ys[:,1]); plt.show(0)
    print('E(0):{0:0.2E}\t E(T):{1:0.2E}\t EP(T):{2:0.2E}, EK(T):{3:0.2E}'.format(ep0,ep1+ek1,ep1,ek1))
  
  if stable:
    if (abs(ep0 - ep1-ek1) > abs(ep0)/100):
      print('again')
      return ftime(R,int(N*1.5),show,lpow,ts[-1])
    
    
  print('E(0):{0:0.2E}\t E(T):{1:0.2E}\t EP(T):{2:0.2E}, EK(T):{3:0.2E}'.format(ep0,ep1+ek1,ep1,ek1))
  return ts[-1], ys[-1,1]
  
 
#integruje a spocte cas dopadu - JINA DISKRETIZACE
def ftimeFib(R, N = 1000, show = False, estT = 1., stable = True, t = None, one_particle_is_fixed = True):
  """\tR - pocatecni vydalenost 
\tN pocet deleni
\tshow vypisuje prubeh
\tstable - jestli vyzadujeme zachovani E
  
  """
  
  #hybou se obe castice?
  dfdt = dfdt1c
  if not one_particle_is_fixed:
    dfdt = dfdt2c
  
  if t is None:
    t = np.linspace(0,estT,N+1)
  else:
    n = len(t)
    nt = np.zeros(n*2-1)
    
    for i in range(n):
      nt[2*i] = t[i]
      
      if i < n-1:
        nt[2*i+1] = (t[i]+t[i+1])/2
    
    
    t = nt
    n = len(t)
    
    c = 1
    cp = 1
    
    tt = []
    for i in range(-1,-n,-1):
      split = False
      
      if -i == c:
        c = c + cp
        cp = -i
        
        split=True
  
      tt.append(t[i])
      
      if split:
        half = float(t[i]-t[i-1])/2
        
       # tt.append(t[i])
        tt.append(t[i]+half)
     # else:
        
      
  
    t = np.array(sorted(tt))
  print(len(t))
  #  print(str(t[:5])+' '+str(t[-5:]))
    
  N = len(t)-1
  y = np.array([R,0])
  
  ys = [y]
  ts = [0]

  dt = np.zeros(N)
  for i in range(N):
    dt[i] = t[i+1]-t[i]

  
  #plt.figure();plt.plot(dt);plt.title('dt');plt.show(0)
  
  ep0 = EP(y, one_particle_is_fixed)
  
  #iteruje dokud nenarazi
  count = 0
  while(y[0] > rlim):
    y = RK4(y,dt[count], dfdt)
    ys.append(y)
    ts.append(ts[-1]+dt[count])
    
    if count < N-1:
      count += 1
  
  ys = np.array(ys)
  
  ep1 = EP(ys[-2], one_particle_is_fixed)
  ek1 = m*np.power(ys[-2,1],2)/2  
  
  if show:
   # plt.figure(); plt.plot(ts,ys[:,0]); plt.show(0)
  #  plt.figure(); plt.plot(ts,ys[:,1]); plt.show(0)
    print('E(0):{0:0.2E}\t E(T):{1:0.2E}\t EP(T):{2:0.2E}, EK(T):{3:0.2E}'.format(ep0,ep1+ek1,ep1,ek1))
  
  
  if stable:
    if (abs(ep0 - ep1-ek1) > abs(ep0)*epsSameE):
      #print('again')
      return ftimeFib(R,show=show,estT=ts[-1],t = t[0:min(len(ts),len(t))], one_particle_is_fixed = one_particle_is_fixed)
      #return ftimeFib(R,show=show,estT=ts[-1],t = ts, one_particle_is_fixed = one_particle_is_fixed)
    
    
  print('E(0):{0:0.2E}\t E(T):{1:0.2E}\t EP(T):{2:0.2E}, EK(T):{3:0.2E}, lenT:{4}'.format(ep0,ep1+ek1,ep1,ek1,len(t)))
  return ts[-1], ys[-1,:]
  

def fv(R, one_particle_is_fixed = True):
    #calc velocity at impact via conservation of energy
  ep0 = -A_c/R
  ep1 = -A_c/rlim
  
  ep0, ep1 = (ep0, ep1) if one_particle_is_fixed else (2*ep0, 2*ep1)
  
  v2 = 2*(ep0-ep1)/m
  v = np.sqrt(v2)
  
  return v
  
N = 15


def timego(N = N, R = 1e-6, folder = None, one_particle_is_fixed = False):
  """provede celou simulaci. vykresli grafy s N delenimi pro pocatecni vzdalenosti az R, vysledek ulozi do slozky folder
dva ruzne vypocty - bud interaguji obe castice nebo se pocita jen interakce jedne castice s ukotvenou (fixed) castici"""

  #R1 = 2.7e-9
  R1= rlim + 1e-7
  R2 = R
  #r = np.linspace(R1,R2, N)
  lr1 = np.log10(R1)
  lr2 = np.log10(R2)
  r = np.logspace(lr1, lr2, N)

  ts = []
  vs = []
  for i in range(N):
    print(r[i])
    
    #odhadovany dopad
    estT = esta*r[i]**3 + estb*r[i]**2 + estc*r[i] + estd
    
    T,y = ftimeFib(r[i],1500, estT = estT, stable =True, one_particle_is_fixed = one_particle_is_fixed)#,show=True)
    v = fv(r[i], one_particle_is_fixed)
    
    ts.append(T)
    vs.append(v)
    print('time: {0:0.2E}; etime: {1:0.2E}'.format(ts[-1], estT  ))
    
  ts2 = np.array(ts)#*1e6
  r2 = np.array(r)#*1e6

  print(vs)

  coef = fitP2(r2,ts2, 1./R2)  

  xsc = 1#e6
  ysc = 1e6
  plt.figure(); plt.plot(xsc*r2,ysc*ts2)
  ax = plt.gca()
  
  ax.set_xscale('log')
  
  plt.xlabel('Initial distance [m]')
  plt.ylabel('Time to impact [1E-6 s]')
  plt.title('Calculation of time to impact')
  #plt.show(0)
  if folder:
    ensure_dir(folder)
    plt.savefig(folder+'/time2impact.tiff', format='tiff')
  
  plt.figure(); plt.plot(xsc*r2,vs)
  ax = plt.gca()
  
  ax.set_xscale('log')
  plt.xlabel('Initial distance [m]')
  plt.ylabel('Impact velocity [m/s]')
  
  plt.title('Calculation of impact velocity')
  
  #plt.show(0)
  if folder:
    plt.savefig(folder+'/VatImpact.tiff', format='tiff')
  
  
	#zobrazi polynomicky fit
  coef = fitP3(r2,ts2, 1./R2)  
  pred = coef[0]*np.power(r2,3)+ coef[1]*np.power(r2,2) + coef[2]*(r2) + coef[3]
  plt.figure(); plt.plot(r2,ts2); plt.plot(r2, pred);plt.title('Polynomial fit'); plt.show(0)

  print('coef of polynomial fit for better prior estimates')
  print(coef)

if __name__ == '__main__':
    timego(folder = 'test')
