import numpy as np 
from . import moleculator as mol
from . import Constants as con
from . import Generators as gen

import scipy.stats as st
import math
import functools as ft

def partial(func, *args, **keywords):
    def newfunc(*fargs, **fkeywords):
        newkeywords = keywords.copy()
        newkeywords.update(fkeywords)
        return func(*(fargs + args), **newkeywords)
    newfunc.func = func
    newfunc.args = args
    newfunc.keywords = keywords
    return newfunc

class Simulation:
	def __init__(self, popsize = 10, bounds = [[0,15000e-9],[0,15000e-9]], divs = 2, glue = False):
		"""
		popsize> initial number of particles
		bounds> particle-box size
		divs> divides space into boxes and considers interaction only between particles in neighbouring boxes (faster)
		glue> (boolean) whether to use heuristic gluing force to ensure that particles stays together
		"""
		
		self.popsize = popsize
    
		self.divs = divs
		self.glue = glue

		self.ro = con.ro0

		self.Temp = con.Temperature
    
		self.bounds = bounds
    
		#no input
		self.init_r = lambda : con.r0
    
		#input is r
		self.init_m = lambda x: np.power(x,3)*math.pi*4./3

		#input is bounds
		self.init_x = gen.random_Uniform
    
		#m is input
		self.init_v = lambda m: [0,0]

		A_c = 8.4e-29
		#swarm is input
		self.calc_Fs = [ partial(mol.force_Hadamar, A_c, 28e-9, 27.417e-9) ]
   
		self.swarm = None
		self.genTime = 0
		
		print("INIT of SIM DONE")
    
	def calc_F(self):
		"""Calculate additive forces"""
		for f in self.calc_Fs:
			f(self.swarm)  
  
	def init(self):
		"""Generate configuration for time 0"""
		self.swarm = mol.Swarm(self.popsize, self.divs, self.glue)
		self.swarm.setB(self.bounds)
	
		size = self.swarm.size_pre
		print("INIT _ size: {0}, ssize:{1}".format(size,self.swarm.size))
		
		rs = []
		ms = []
		xs = []
		vs = []
		for i in range(self.swarm.size_pre):
			rs.append(self.init_r())
			ms.append(self.init_m(rs[-1]))
			xs.append(self.init_x(self.bounds))
			vs.append(self.init_v(ms[-1]))

		print(ms)
		print(xs)
		print(vs)
			
		self.swarm.r = rs
		self.swarm.m = ms
		self.swarm.setX( xs )
		self.swarm.setV( vs )    
		
		self.time = 0
		
		
		
		#calculation of generation mean time
		bounds = self.bounds
		#1 per 15ns per um**2
		self.surface = (bounds[0][1]-bounds[0][0])*(bounds[1][1]-bounds[1][0])
		print("surface : "+str(self.surface))
		per15nsec = self.surface/1e-12
		persec = per15nsec / 15e-9    
		#naplochu = 38*self.surface*1e12
		self.genlambda = 1./persec
		
		self.genTime = np.random.exponential(self.genlambda)
		
		print("mean time to generation of particle: "+str(self.genlambda))
		
		self.swarm.debug_print()
		
		print("initDONE")

	def getVF(self):
		"""Get Velocity and Force tuple for Runkge-Kutt method"""
		# this probably slows down the simulation - better would be to code R-K in C++
		# there is a way to send function pointers into C via Pybind11 to calculate forces
		v = np.array([ np.array(self.swarm.getV(0)) , np.array(self.swarm.getV(1))  ] )
		f = np.array([ np.array(self.swarm.getF(0)) , np.array(self.swarm.getF(1))  ] )

		return v,f

	def iterate(self, dt = 0.1):
		"""Iterate by dt"""
		#print("ITERUJU {0} parts, t:{1}".format(self.swarm.size,self.time))
		self.time += dt
    
		debugPR = False
		
		#if time to generate new particle has come>>>
		if self.time > self.genTime:
			#print("rewhatever")
			
			#if internal datastructures has to be resized>>>>
			if self.swarm.size >= self.swarm.size_pre:
				oldsize = self.swarm.size_pre
				s = self.swarm.size
				
				sv = [self.swarm.getV(0) , self.swarm.getV(1)]
				sx = [self.swarm.getX(0),self.swarm.getX(1)]
				sr = self.swarm.r
				sm = self.swarm.m 
				
				self.swarm.inflate()
        
        
				ssv = np.array([self.swarm.getV(0) , self.swarm.getV(1)])
				ssx = np.array([self.swarm.getX(0),self.swarm.getX(1)])
				ssr = self.swarm.r
				ssm = self.swarm.m         
				
				ssv[0][:s] = sv[0][:]
				ssv[1][:s] = sv[1][:]
				ssx[0][:s] = sx[0][:]
				ssx[1][:s] = sx[1][:]
				ssr[:s] = sr
				ssm[:s] = sm
        
				for i in range(s,self.swarm.size_pre):
					ssr[i]=self.init_r()
					ssm[i]=self.init_m(ssr[i])
					ssx[:,i]=self.init_x(self.bounds)
					ssv[:,i]=self.init_v((ssm[i]))
        
        
				self.swarm.setX(ssx)
				self.swarm.setV(ssv)
				self.swarm.r = ssr
				self.swarm.m = ssm
				
				  
				#self.swarm.debug_printF()
					  
				debugPR = True
      
			#just increase current population size >> all particles are being generated in advance
			self.swarm.size +=  1
			self.genTime = self.time + np.random.exponential(self.genlambda)
    
    
		#the integration
		##do not forget to call calcDists
		euler = False
		if euler:
			self.swarm.calcDists()
			self.calc_F(dt)
			self.swarm.integrate(dt)
		
		#runge-kutt
		else:
			dh = dt/2
			  
			self.swarm.calcDists()
			self.calc_F()
			x0 = [self.swarm.getX(0),self.swarm.getX(1)]
			kv1 , kf1 = self.getVF()
			  
			self.swarm.integrate(dh)
			self.swarm.enforceBounds()

			#self.swarm.debug_printF()

			self.swarm.calcDists()
			self.calc_F()
			  
			kv2 ,kf2 = self.getVF()
			  
			self.swarm.setX(x0)
			self.swarm.setV(kv2)
			self.swarm.setF(kf2)
			  
			self.swarm.integrate(dh)
			self.swarm.enforceBounds()      
			  
			#self.swarm.debug_printF()      
			  
			self.swarm.calcDists()
			self.calc_F()
			  
			kv3 , kf3 = self.getVF()
			
			self.swarm.setX(x0)
			self.swarm.setV(kv3)
			self.swarm.setF(kf3)
			  
			self.swarm.integrate(dt)
			self.swarm.enforceBounds()   
			
			#self.swarm.debug_printF()    
			 
			self.swarm.calcDists()
			self.calc_F()
			 
			kv4 , kf4= self.getVF()
					
			kv = (kv1+2*kv2+2*kv3+kv4)/6.
			kf = (kf1+2*kf2+2*kf3+kf4)/6.
			 
			self.swarm.setX(x0)
			self.swarm.setV(kv)
			self.swarm.setF(kf)
			self.swarm.integrate(dt)


		#check position and velicity extremes
		self.swarm.enforceBounds()
		self.checkV2()
    
		#if alltogether -> timewarp and generate new particle
		glued = self.swarm.getGlued()
		allglued = True
		for i in range(self.swarm.size):
			if (glued[i] == "0"):
				allglued = False
		if allglued:
			print('all glued -> jumpin')
			self.time = self.genTime
    

	def checkV2(self):
		"""If V is improbable (regarding Maxwell-Boltzmann), decrease it"""
		a = np.sqrt(con.k_boltzmann*self.Temp/self.swarm.m[0])
		#crit = 1e20
		q99 = st.maxwell.ppf(0.9999, 0 , a)
		mmean = np.sqrt(8*con.k_boltzmann*self.Temp/(np.pi*self.swarm.m[0])) #st.maxwell.mean(0,a)
		
		self.swarm.correctV2(mmean,q99)

	def getE(self):
		"""Get Energy if energy function is specified"""
		E = 0
		E += self.swarm.getEK()
		#E += self.calc_EP(self.swarm)/1e12
		
		return E 
		
	def load(self, fname, pos = -1):
		"""Load configuration from file"""
		fize = 0
		with open('res/'+fname+'.txt','r') as f:
			for li, line in enumerate(f):
				pass
			fize = li+1


		with open('res/'+fname+'.txt','r') as f:
			for li, line in enumerate(f):


				if li == (pos*3)%fize:
      
					l1s = line.split(' ')
					size = int(l1s[1])
					  
					print(size)
					  
					self.swarm = mol.Swarm( size , self.divs, self.glue)
					self.swarm.setB(self.BB)

					  
					self.init()   
					self.time = float( l1s[0] )       
         
				if li == (pos*3)%fize+1:
          
					xs0 = []
					xs1 = []
					lb = line.strip(' \n')
					l2s = lb.split(' ')
					for i in range( self.swarm.size):
						xs0.append( float( l2s[i] ) )
						xs1.append( float( l2s[i+size] ) )      
				  
					self.swarm.setX([ xs0 , xs1])
      
				if li == (pos*3)%fize+2:
                
					vs0 = []
					vs1 = []
				  
					lb = line.strip(' \n')
					l3s = lb.split(' ')
			  
					for i in range( self.swarm.size):
						vs0.append( float( l3s[i] ) )
						vs1.append( float( l3s[i+size] ) )
				  
					self.swarm.setV([ vs0 , vs1])
      
	def plot(self, name = None):
		"""Show configuration"""
		fig = plt.figure()
    
		scale = 1e6
    
		x1 = self.swarm.getX(0)
		x2 = self.swarm.getX(1)

		b = self.swarm.getB()
		bdif = [b[0][1]-b[0][0], b[1][1]-b[1][0]]
		bmean = [(b[0][1]+b[0][0] )/2, (b[1][1]+b[1][0])/2]
		bdif[0]*=1.1
		bdif[1]*=1.1

		partR = self.r*0.9
    
		b2 = np.zeros((2,2))
		for i in range(2):
			b2[i,0] = bmean[i]-bdif[i]/2
			b2[i,1] = bmean[i]+bdif[i]/2

		b2*=scale

		ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
                       xlim=(b2[0,0], b2[0,1]), ylim=(b2[1,0], b2[1,1]))
		ax.set_title('Time:{0:0.2E} [s]'.format(sim.time))
  
		#color = (0.6,0,0.6)
		color = 'lightcyan'
  
		for i in range(self.swarm.size):
			pb = plt.Circle((x1[i]*scale,x2[i]*scale), radius=partR*scale*1.05, color='k', edgecolor = 'k')#, fill = False)
			p = plt.Circle((x1[i]*scale,x2[i]*scale), radius=partR*scale, color=color, edgecolor = 'k')#, fill = False)
			ax.add_patch(pb)
			ax.add_patch(p)
      
		plt.xlabel('X [1E-6 m]')
		plt.ylabel('Y [1E-6 m]')
      
      
		if name:
			fig.savefig('totalout/'+name+'.tiff',format='tiff')
		plt.show(0)
