import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np

class ohno:
  def __init__(self, cs):
    self.cs = cs

def panimate(sim, dt = 0.01, frames = 600, stepsperframe = 250, fname = 'particle_box', reinit = True):
  """Computes and saves new animation into fname
  reinit decides whether to call simulation.init() (False is used when sim is loaded from file and continued)"""
  print(fname)
  
  if reinit:
    print('initing in ANI init')
    sim.init()
  
  # set up figure and animation
  fig = plt.figure()
  b = sim.swarm.getB()
  
#  fig.subplots_adjust(left=0, right=1, bottom=0, top=1)

  bdif = [b[0][1]-b[0][0], b[1][1]-b[1][0]]
  bmean = [(b[0][1]+b[0][0] )/2, (b[1][1]+b[1][0])/2]
  bdif[0]*=1.1
  bdif[1]*=1.1

  #partR = sim.r
  
  b2 = np.zeros((2,2))
  for i in range(2):
    b2[i,0] = bmean[i]-bdif[i]/2
    b2[i,1] = bmean[i]+bdif[i]/2

  ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
                     xlim=(b2[0,0], b2[0,1]), ylim=(b2[1,0], b2[1,1]))
  ax.set_title('time:{0:0.2E}'.format(sim.time))

  rect = plt.Rectangle((b[0][0],b[1][0]), b[0][1]-b[0][0], b[1][1]-b[1][0], lw = 0.5, fc='none', fill=False)
  ax.add_patch(rect)

  # particles holds the locations of the particles
  particles = []
  texts = []
  
  radiuses = sim.swarm.r
  for i in range(sim.swarm.size):
    p = plt.Circle((0,0), radius= radiuses[i], color = 'r') #sim.r)
    particles.append(p)
    ax.add_patch(particles[i])
  
  curr_size = sim.swarm.size
  
  #im not sure why i needed this
  cusi = ohno(curr_size)
  
  t = ax.text(-0.05,b[1][1]+5,'time: 0')
  #t1 = ax.text(-0.05,b[1][1]+0.15,'E:0')
  texts.append( t )
  #texts.append( t1 )

  def init():
      """initialize animation"""
      #global sim, boss
      
      print('sim time: {0}'.format(sim.time))
      
      #~ particles = boss['particles']
      #~ rect = boss['rect']
      
      xys = []
      xys.append(sim.swarm.getX(0))
      xys.append(sim.swarm.getX(1))
      
      vys = [ sim.swarm.getV(0) , sim.swarm.getV(1)]
      
      
      for i in range(sim.swarm.size):
        particles[i].center = (xys[0][i],xys[1][i])
      
      texts[0].set_text('time: {0:.2E}'.format( sim.time ))
      #texts[1].set_text('E: {0:.2E}'.format( sim.getE() ))
      
      with open('res/'+fname+'.txt','w') as f:
        f.write('{0:.2E} {1}'.format(sim.time, sim.swarm.size))
        f.write('\n')
        for k in range(2):
          for j in range(sim.swarm.size):
            f.write(' {0:.3E}'.format(xys[k][j]))
        f.write('\n')
        for k in range(2):
          for j in range(sim.swarm.size):
            f.write(' {0:.3E}'.format(vys[k][j]))
            
        f.write('\n')
      
      print("done ANI init")
      return rect, 
      
  def ap(part,x,y):
    part.center = (x,y)
    return part,

  def animate(i, parts, txts, cusi):
      """perform animation step"""
      
      for j in range(stepsperframe):
        sim.iterate(dt)

      xys = [ sim.swarm.getX(0) , sim.swarm.getX(1)]
      vys = [ sim.swarm.getV(0) , sim.swarm.getV(1)]
      
      if i % 100 == 0:
        print('frame {0} of {1}; popsize {2}'.format(i,frames,sim.swarm.size))
        #sim.swarm.debug_print()

      radiuses = sim.swarm.r
      if sim.swarm.size > cusi.cs:
        for j in range(cusi.cs,sim.swarm.size):
          p = plt.Circle((0,0), radius = radiuses[j], color = 'r' )#sim.r)
          parts.append(p)
          ax.add_patch(p)
        cusi.cs = sim.swarm.size
      
      with open('res/'+fname+'.txt','a') as f:        
        f.write('{0:.2E} {1}'.format(sim.time, sim.swarm.size))
        f.write('\n')
        for k in range(2):
          for j in range(sim.swarm.size):
            f.write(' {0:.3E}'.format(xys[k][j]))
        f.write('\n')
        for k in range(2):
          for j in range(sim.swarm.size):
            f.write(' {0:.3E}'.format(vys[k][j]))
            
        f.write('\n')
      
      for j in range(cusi.cs):#sim.swarm.size):
        #ap(parts[j],xys[0][j],xys[1][j])
        parts[j].center = (xys[0][j], xys[1][j])
        
      ax.set_title('time:{0:0.2E}'.format(sim.time))  
        
      txts[0].set_text('time: {0:.2E}'.format( sim.time ))
      #txts[1].set_text('E: {0:.2E}'.format( sim.getE() ))
        
      return rect,



  ani = animation.FuncAnimation(fig, animate, fargs = (particles,texts,cusi,), frames=frames,
                              interval=1, blit=True, init_func=init, repeat = False)


# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html
  ani.save('res/'+fname+'.mp4', fps=15, extra_args=['-vcodec', 'libx264'])

  print('IM DONE')
  ## DO NOT USE show !!!!!!
  #plt.show(0)
  return

 
