import numpy as np

def random_Uniform(bounds = [0,1]):
	dim = len(bounds)
	if dim > 1:
		r = np.random.rand(dim)
		for i in range(dim):
			r[i] = r[i]*(bounds[i][1]-bounds[i][0]) + bounds[i][0]
    
		return r

	a=np.random.rand()*(bounds[1]-bounds[0])+bounds[0]
	return a 

def random_Maxwell(Tdm ,dim=2):#swarm, T, dim = 2):
	A = np.sqrt(k_boltzmann*Tdm/m0)
  
 # m = swarm.m
  
	eks = st.maxwell.rvs(0,a,size)
  
	v = np.zeros((dim,size))
	for i in range(size):
		vnorm = eks[i]
		fi = np.random.rand()*2*np.pi
		v[0][i] = vnorm*np.cos(fi)
		v[1][i] = vnorm*np.sin(fi)
  
	return v
