#include "model.h"

#include <iostream>
#include <vector>
#include <cmath>

#include <random>

using namespace std;
const double PI  =3.141592653589793238463;
const float Bsigma = 66200;
const float Bsd = sqrtf(Bsigma);

static void calcNewton(Swarm &s, float kappa = 1 )
{
  float sum = 0;
  #pragma omp parallel for schedule(dynamic,2) reduction(+:sum)
  for (size_t i = 0; i < s.size; i++)
  {
    float Fsum = 0;
    for (size_t j = i+1 ; j < s.size; j++)
    {
      vector<float> diff(dim);
      float n2 = 0;
      for(int k = 0 ; k < dim; k++)
      {
        float a = s.x.co[k][j] - s.x.co[k][i];
        diff[k] = a;
        n2 += a*a;
      }
      
      float coeff=0;
      if (n2 > 1e-5)
      {
        float n1 = sqrtf(n2);
        coeff = kappa * s.m[i] * s.m[j] / (n2*n1);
      }
       
      for (int k = 0 ; k < dim ; k++)
      {
        s.F.co[k][i] += coeff * diff[k];
        s.F.co[k][j] -= coeff * diff[k];
      }
    }
  }
}

static void calcLJOLD(Swarm &s, float r0 = 1, float eps = 1, int n =6 )
{
  float sum = 0;
  #pragma omp parallel for schedule(dynamic,2) reduction(+:sum)
  for (size_t i = 0; i < s.size; i++)
  {
    float Fsum = 0;
    for (size_t j = i+1 ; j < s.size; j++)
    {
      vector<float> diff(dim);
      float n2 = 0;
      for(int k = 0 ; k < dim; k++)
      {
        float a = s.x.co[k][j] - s.x.co[k][i];
        diff[k] = a;
        n2 += a*a;
      }
      
      float coeff=0;
      if (n2 > 1e-5)
      {
        float n1 = sqrtf(n2);
        coeff = n*eps *( pow(r0/n1,n) - 2*pow(r0/n1,2*n)  )/n1;
      }
       
      for (int k = 0 ; k < dim ; k++)
      {
        s.F.co[k][i] += coeff * diff[k];
        s.F.co[k][j] -= coeff * diff[k];
      }
    }
  }
}
static void calcLJ(Swarm &s,  float r0 = 1, float eps = 1, int n =6 )
{

  #pragma omp parallel for schedule(dynamic,2)
  for (size_t i = 0; i < s.size; i++)
  {
    float Fsum = 0;
    vector<size_t> pos(dim);
    for (int l = 0 ; l < dim ; l++)
    {
      float px = s.hex->n*(s.x.co[l][i]-s.bounds[l][0])/(s.bounds[l][1]-s.bounds[l][0]);
      pos[l] = size_t(floor(px));
    }
      
    vector<size_t> neis = s.hex->getNei(pos);
    for (int j = 0 ; j < neis.size(); j++)
    {
      size_t ind = neis[j];
      if (ind >= i) continue;

      float n2 = s.dists[dim][i][ind];
      float coeff=0;
      if (n2 > 1e-10)
      {
        float n1 = s.dists[dim+1][i][ind];
        coeff = 2*n*eps *( pow(r0/n1,n) - pow(r0/n1,2*n)  )/n2;
      }
      
      
      for (int k = 0 ; k < dim ; k++)
      {
        s.F.co[k][i] += coeff * s.dists[k][i][ind];
        s.F.co[k][ind] -= coeff * s.dists[k][i][ind];
      }

    }
  }
}


//USE THIS
static void calcHadamar(Swarm &s,  float A = 1, float r0 = 1, float r1 = 0.9)
{

  //float C = A/(r0*r0*logf(r0/r1));
  //float rm = (r0+r1) / 2;

  float r02 = powf(r0,2);
  float r12 = powf(r1,2);
  float dh = A/((r0-r1)*(r0-r1));
  float dr = r12*r02/(r02-r12);
  float C2 = -dh*dr;    //(A*r12*r02)/((r02-r12)*(r0-r1)*(r0-r1));
  float B = -C2/r12;
  
  //cout << dh << " " << dr<<" " <<C2 << " " <<B<<endl;

  #pragma omp parallel for schedule(dynamic,2)
  for (size_t i = 0; i < s.size; i++)
  {
    if (s.glued[i]) //only local forces applied on glued particles
    {
      vector<size_t> pos(dim);
      for (int l = 0 ; l < dim ; l++)
      {
        float px = s.hex->n*(s.x.co[l][i]-s.bounds[l][0])/(s.bounds[l][1]-s.bounds[l][0]);
        pos[l] = size_t(floor(px));
      }
        
      vector<size_t> neis = s.hex->getNei(pos);
      for (int j = 0 ; j < neis.size(); j++)
      {
        size_t ind = neis[j];
        if (ind >= i) continue;

        float n2 = s.dists[dim][i][ind];
        float n1 = s.dists[dim+1][i][ind];
        float coeff = 0;
        
        
        if (n1 > r0)
        {
          float x = n2 - 2*n1*r1  + r12;
          coeff = A/(x*n1);
        }
        else
        {
          //float x = n1/rm;
          //coeff = C*logf( x )/n1;
          coeff = (C2/n2 + B)/n1;
          
        }
        
        
        for (int k = 0 ; k < dim ; k++)
        {
          s.F.co[k][i] += coeff * s.dists[k][i][ind];
          s.F.co[k][ind] -= coeff * s.dists[k][i][ind];
        }

      }
    }
    else
    {
      for (size_t j = 0 ; j < s.size; j++)
      {
        if (j == i) continue;
        size_t ind = j;

        float n2 = s.dists[dim][i][ind];
        float n1 = s.dists[dim+1][i][ind];
        float coeff = 0;
        
        
        if (n1 > r0)
        {
          float x = n2 - 2*n1*r1  + r12;
          coeff = A/(x*n1);
        }
        else
        {
          //float x = n1/rm;
          //coeff = C*logf( x )/n1;
          coeff = (C2/n2 + B)/n1;
          
        }
        
        
        for (int k = 0 ; k < dim ; k++)
        {
          s.F.co[k][i] += coeff * s.dists[k][i][ind];
          s.F.co[k][ind] -= coeff * s.dists[k][i][ind];
        }

      }
    }
  }  
}

static void calcHadamarIso(Swarm &s,  float A = 1, float r0 = 1, float r1 = 0.9)
{

  //float C = A/(r0*r0*logf(r0/r1));
  //float rm = (r0+r1) / 2;

  float r02 = powf(r0,2);
  float r12 = powf(r1,2);
  float dh = A/((r0-r1)*(r0-r1));
  float dr = r12*r02/(r02-r12);
  float C2 = -dh*dr;    //(A*r12*r02)/((r02-r12)*(r0-r1)*(r0-r1));
  float B = -C2/r12;
  
  //cout << dh << " " << dr<<" " <<C2 << " " <<B<<endl;

  #pragma omp parallel for schedule(dynamic,2)
  for (size_t i = 0; i < s.size; i++)
  {
    float Fsum = 0;
    vector<size_t> pos(dim);
    for (int l = 0 ; l < dim ; l++)
    {
      float px = s.hex->n*(s.x.co[l][i]-s.bounds[l][0])/(s.bounds[l][1]-s.bounds[l][0]);
      pos[l] = size_t(floor(px));
    }
      
    vector<size_t> neis = s.hex->getNei(pos);
    for (int j = 0 ; j < neis.size(); j++)
    {
      size_t ind = neis[j];
      if (ind >= i) continue;

      float n2 = s.dists[dim][i][ind];
      float n1 = s.dists[dim+1][i][ind];
      float coeff = 0;
      
      
      if (n1 > r0)
      {
        float x = n2 - 2*n1*r1  + r12;
        coeff = A/(x*n1);
      }
      else
      {
        //float x = n1/rm;
        //coeff = C*logf( x )/n1;
        coeff = (C2/n2 + B)/n1;
        
      }
      
      
      for (int k = 0 ; k < dim ; k++)
      {
        s.F.co[k][i] += coeff * s.dists[k][i][ind];
        s.F.co[k][ind] -= coeff * s.dists[k][i][ind];
      }

    }
  }  
}

static void calcHadamarGlue(Swarm &s,  float A = 1, float r0 = 1, float r1 = 0.9)
{

  //float C = A/(r0*r0*logf(r0/r1));
  //float rm = (r0+r1) / 2;

  float r02 = powf(r0,2);
  float r12 = powf(r1,2);
  float dh = A/((r0-r1)*(r0-r1));
  float dr = r12*r02/(r02-r12);
  float C2 = -dh*dr;    //(A*r12*r02)/((r02-r12)*(r0-r1)*(r0-r1));
  float B = -C2/r12;
  
  //cout << dh << " " << dr<<" " <<C2 << " " <<B<<endl;

  #pragma omp parallel for schedule(dynamic,2)
  for (size_t i = 0; i < s.size; i++)
  {
    float Fsum = 0;
    vector<size_t> pos(dim);
    for (int l = 0 ; l < dim ; l++)
    {
      float px = s.hex->n*(s.x.co[l][i]-s.bounds[l][0])/(s.bounds[l][1]-s.bounds[l][0]);
      pos[l] = size_t(floor(px));
    }
      
    vector<size_t> neis = s.hex->getNei(pos);
    for (int j = 0 ; j < neis.size(); j++)
    {
      size_t ind = neis[j];
      if (ind >= i) continue;

      float n2 = s.dists[dim][i][ind];
      float n1 = s.dists[dim+1][i][ind];
      float coeff = 0;
      
      
      if (n1 > r0)
      {
        float x = n2 - 2*n1*r1  + r12;
        
        if (s.glue[i][ind])
          coeff = x*A/(dh*(r0-r1)*n1);
        else
          coeff = A/(x*n1);
      }
      else
      {
        //float x = n1/rm;
        //coeff = C*logf( x )/n1;
        coeff = (C2/n2 + B)/n1;
        s.glue[i][ind] = true;
      }
      
      
      for (int k = 0 ; k < dim ; k++)
      {
        s.F.co[k][i] += coeff * s.dists[k][i][ind];
        s.F.co[k][ind] -= coeff * s.dists[k][i][ind];
      }

    }
  }  
}

static void calcBrown(Swarm &s)
{
  float sig = Bsd;
  default_random_engine generator;
  normal_distribution<double> distribution(0.0,1);
  uniform_real_distribution<double> uni(0.0,2*PI);
  
//  #pragma omp parallel for schedule(dynamic,2)
  for (size_t i = 0; i < s.size; i++)
  {
    float alfa = uni(generator);
    float val = distribution(generator) * s.m[i] * sig;
    s.F.co[0][i] += sin(alfa)*val;
    s.F.co[1][i] += cos(alfa)*val;
    //cout << "val: " <<val << " alfa: "<<alfa<< endl;
  }  
}

static float calcLJE(Swarm &s, float r0 = 1, float eps = 1, int n =6 )
{
  float sum = 0;
  #pragma omp parallel for schedule(dynamic,2) reduction(+:sum)
  for (size_t i = 0; i < s.size; i++)
  {
    float Fsum = 0;
    for (size_t j = 0 ; j < i; j++)
    {
      float n2 = s.dists[dim][i][j];
      float coeff=0;
      if (n2 > 1e-5)
      {
        float n1 = s.dists[dim+1][i][j];
        coeff = eps *( pow(r0/n1,2*n) - 2*pow(r0/n1,n)  );
      }
      sum += coeff;
    }
  }
  
  return sum;
}
