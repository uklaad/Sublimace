#pragma once

#include <iostream>
#include <vector>
#include <string>

#include <sstream>
#include <iomanip>

#include <math.h>

using namespace std;

#define dim 2

class Space
{
  public:
  vector<vector<float> > co;
  size_t size;
  
  string name;
  
  void init(string name_ = "-1")
  {
    if (name_!="-1")
      name = name_;
      
    co.resize(dim);
    
    for(int i = 0 ; i < dim; i++)
    {
      co[i].resize(size);
    }    
  }
  
  Space(size_t size_=0, string name_ = "Foo")
  {
    size = size_;
    init(name_);
  }

  void setCO(vector<vector<float> > &in)
  {
    
    size_t acs = in[0].size();
    //cout << "size for setCO is: " <<acs <<endl;
   
//    cout << "settin CO: mysize: "<<size<<"  , insize:"<<acs;
    #pragma omp parallel for
    for(size_t i = 0 ; i < acs; i++)
    for(int k = 0 ; k < dim ; k++)
    {
      co[k][i] = in[k][i];      
//      cout << "got one" <<endl;
    }
    
//    cout << "  .... set co done"<<endl;
  }
  
  void setCONST(float x)
  {
    #pragma omp parallel for
    for(size_t i = 0 ; i < size; i++)
    for(int k = 0 ; k < dim ; k++)
    {
      co[k][i] = x;      
    }    
  }
  
  Space(vector<vector<float> > &in, string name_ = "Foo") : Space(in[0].size(),name_)
  {
    setCO(in);
  }
  
  vector<float> getCO(int ind)
  {
	vector<float> out(size);
	#pragma omp parallel for
	for(size_t i=0; i < size; i++)
		out[i] = co[ind][i];
	return out;
  }
  
  void inflate()
  {
    size *= 2;
    for (int k = 0 ; k < dim ; k++)
    {
      co[k].resize(size);
    }
  }
  
  void addParticle(vector<float> &p)
  {
    if (size + 1 >= co[0].size())
      for (int k = 0 ; k < dim ; k++)
        co[k].resize(co[k].size()*2);
      
    for (int i = 0 ; i < dim ; i++)
      co[i][size] = p[i];
      
    size = size + 1;
  }
  void addParticle(float x)
  {
    vector<float> p(dim);
    for (int i = 0 ; i < dim ; i++)
      p[i] = x;
      
    addParticle(p);
  }
  
  ~Space()
  {
    for(int i = 0 ; i < dim; i++)
    {
      co[i].clear();
    }
  }
  
};

class HexGrid
{
  public:
  size_t n;
  size_t size;
  vector< vector<size_t> > indexes;
  vector<size_t> mul;
  
  HexGrid(size_t hex_div=100)
  {
    n = hex_div;
    size = size_t(pow(n,dim));
    indexes.resize(size);
    mul.resize(dim);
    size_t m = 1;
    for(int i =0 ; i < dim; i++)
    {
      mul[i] = m;
      m*=n;
    }
    resetMe();
  }
  
  void resetMe()
  {
    #pragma omp parallel for
    for(size_t i = 0; i<size; i++)
    {
      indexes[i].clear();
    }
  }
  

  void append(vector<vector<size_t> > &poss)
  {
//    cout << "hex appending ... "<<endl;
    size_t sin = poss.size();
    
    vector<size_t> inds(sin);
    #pragma omp parallel for
    for(size_t i = 0 ; i < sin; i++)
    {
      size_t index = 0;
      for (int k = 0 ; k < dim ; k++)
      {
        if (poss[i][k] >= n)
          index += (n-1)*mul[k];
        else
          index += poss[i][k]*mul[k];
      }
      if (index >= size)
        cout << "appending to index "<<index << "of "<< size <<endl;    
      inds[i] = index;
    }
    for (int i = 0 ; i < sin; i++) 
      indexes[inds[i]].push_back(i);
    
  }
  

  
  vector<size_t> get(vector<size_t> &pos)
  {
    size_t index = 0;
    for (int i = 0 ; i < dim ; i++)
    {
      index += pos[i]*mul[i];
    }
    return indexes[index];
  }
  
  void pushNei(vector<size_t> &out, vector<size_t> &pos, int dir, int w)
  {
//    cout << "pushing nei" << endl;
    if (dir == dim)
    {
      vector<size_t> a = get(pos);
      out.insert(out.end(), a.begin(), a.end());
    }
    else
    {
      for(int i = -w; i<w+1;i++)
      {
        vector<size_t> npos(pos);
        npos[dir] += i;
        
        if (npos[dir] >= n) continue;
        if (npos[dir] < 0) continue;
        pushNei(out, npos, dir+1, w);
      }
    }   
  }
  
  vector<size_t> getNei(vector<size_t> &pos, int w = 1)
  {
    vector<size_t> out;// = get(pos);
    
    //~ cout<<"printing neis1"<<endl;
    //~ for (size_t i = 0; i < out.size(); i++)
    //~ {
      //~ cout << out[i] << " ";
    //~ }
    //~ cout<<endl;

    pushNei(out, pos, 0, w);
    
    //~ cout<<"printing neis"<<endl;
    //~ for (size_t i = 0; i < out.size(); i++)
    //~ {
      //~ cout << out[i] << " ";
    //~ }
    //~ cout<<" done"<<endl;
    
    return out;
  }
  
  void debug_print()
  {
    for (size_t i = 0 ; i < size; i++)
    {
      if (indexes[i].size() > 0)
        cout<< i << " " <<indexes[i].size() << endl;
    }
  }
  
  ~HexGrid()
  {
    resetMe();
    indexes.clear();
    mul.clear();
  }
};

class Swarm
{
  public:
  
  float Energy;
  
  Space x;
  Space v;
  Space newv;
  Space F;
  
  vector<float> m;
  vector<float> r;
  
  float bounds[dim][2];
  
  size_t size;
  size_t size_pre;
  
  HexGrid* hex;
  size_t hex_n;
  
  bool u_glue;
  vector< vector<bool> > glue;
  vector<bool> glued;
  
  vector< vector< vector<float> > > dists;
  int nodists = dim+2;
  
  float rlim;
  
  Swarm(size_t size_=0, size_t hex_div = 10, bool use_glue = false, float r_lim = 27.4e-9)
  {
    size = size_;
    size_pre = size*2;//*10;
    Energy = 0;
    
    rlim = r_lim;
    
    x.size = size_pre;
    v.size = size_pre;
    newv.size = size_pre;
    F.size = size_pre;
    
    x.init("X");
    v.init("V");
    newv.init("newV");
    F.init("F");
    
    m.resize(size_pre);
    r.resize(size_pre);
    
    hex_n = hex_div;
    hex = new HexGrid(hex_n);
    
    
    u_glue = use_glue;
    if (u_glue){
      glue.resize(size_pre);
      for (size_t i = 0 ; i < size_pre; i++)
      {
        glue[i].resize(size_pre);
      }
    }
    glued.resize(size_pre);
    for (size_t i = 0 ; i < size_pre; i++)
    {
		glued[i] = 0;
	}
    
    
    dists.resize(nodists);
    for (int j = 0 ; j < nodists; j++)
    {
      dists[j].resize(size_pre);
      for (int i = 0 ; i < size_pre; i++)
      {
        dists[j][i].resize(size_pre);//i);
      }
    }
    
    
    for (int i = 0 ; i < dim ; i++)
    {
      bounds[i][0] = 0;
      bounds[i][1] = 1500;
    }
    
  }
  
  vector<char> getGlued()
  {
	  vector<char> out(size_pre);
	  #pragma omp parallel for
	  for(size_t i = 0 ; i < size_pre; i++)
	  {
		  if (glued[i])
			out[i] = '1';
		  else
			out[i] = '0';
	  }
	  return out;
  }
  
  void addParticle(vector<float> x_, vector<float> v_, float m_, float r_)
  {
    x.addParticle(x_);
    v.addParticle(v_);
    newv.addParticle(0);
    F.addParticle(0);
    
    if (size+1>=m.size())
    {
      size_t s = m.size()*2;
      m.resize(s);
      r.resize(s);
      
      for (int j = 0 ; j < nodists; j++)
      {
        dists[j].resize(s);
        for (int i = s/2 ; i < s ; i++)
          dists[j][i].resize(i);
      }
    }
    m[size] = m_;
    r[size] = r_;
    
    size += 1;
    
  }
  
  void calcDists()
  {
	  //calculates distances between particles w/ grid
//    cout << "calculating distances" << endl;
    F.setCONST(0);
    #pragma omp parallel for schedule(dynamic,20)
    for (int i = 0 ; i < size; i++)
    {
      if (glued[i])
      {
        vector<size_t> pos(dim);
        for (int l = 0 ; l < dim ; l++)
        {
          float px = hex->n*(x.co[l][i]-bounds[l][0])/(bounds[l][1]-bounds[l][0]);
          pos[l] = size_t(floor(px));
        }
        
        vector<size_t> neis = hex->getNei(pos);
        
        for (int j = 0 ; j < neis.size(); j++)
        {
          size_t ind = neis[j];
          if (ind >= i) continue;
          for (int k = 0; k < dim; k++)
          {
            dists[k][i][ind] = x.co[k][ind] - x.co[k][i];
          }
        }
      }
      else
      {
        for (size_t j = 0 ; j < size; j++)
        {
          if (i == j) continue;
          
          for (int k = 0; k < dim; k++)
          {
            dists[k][i][j] = x.co[k][j] - x.co[k][i];
          }
        }
      }
    }
    
    #pragma omp parallel for schedule(dynamic,20)
    for (int i = 0 ; i < size; i++)
    {   
      if (glued[i])
      {   
        vector<size_t> pos(dim);
        for (int l = 0 ; l < dim ; l++)
        {
          float px = hex->n*(x.co[l][i]-bounds[l][0])/(bounds[l][1]-bounds[l][0]);
          pos[l] = size_t(floor(px));
        }
        vector<size_t> neis = hex->getNei(pos);
        for (int j = 0 ; j < neis.size(); j++)
        {
          size_t ind = neis[j];
          if (ind >= i) continue;

          float r2 = 0;
          for (int k =0; k < dim; k++)
            r2 += dists[k][i][ind]*dists[k][i][ind];
          
          dists[dim][i][ind] = r2;
          dists[dim+1][i][ind] = sqrtf(r2);
        }
      }
      else
      {
        for (size_t j = 0 ; j < size; j++)
        {
          if (i == j) continue;
          
          float r2 = 0;
          for (int k =0; k < dim; k++)
            r2 += dists[k][i][j]*dists[k][i][j];
          
          dists[dim][i][j] = r2;
          dists[dim+1][i][j] = sqrtf(r2);
          
          if (dists[dim+1][i][j] < rlim) glued[i] = true;
          
        }
      }
    }
    
  }
  
  
  void calcDistsIso()
  {
	  //doesnt use grid
//    cout << "calculating distances" << endl;
    F.setCONST(0);
    #pragma omp parallel for schedule(dynamic,20)
    for (int i = 0 ; i < size; i++)
    {
      vector<size_t> pos(dim);
      for (int l = 0 ; l < dim ; l++)
      {
        float px = hex->n*(x.co[l][i]-bounds[l][0])/(bounds[l][1]-bounds[l][0]);
        pos[l] = size_t(floor(px));
      }
      
      vector<size_t> neis = hex->getNei(pos);
      
      for (int j = 0 ; j < neis.size(); j++)
      {
        size_t ind = neis[j];
        if (ind >= i) continue;
        for (int k = 0; k < dim; k++)
        {
          dists[k][i][ind] = x.co[k][ind] - x.co[k][i];
        }
      }
    }
    
    #pragma omp parallel for schedule(dynamic,20)
    for (int i = 0 ; i < size; i++)
    {      
      vector<size_t> pos(dim);
      for (int l = 0 ; l < dim ; l++)
      {
        float px = hex->n*(x.co[l][i]-bounds[l][0])/(bounds[l][1]-bounds[l][0]);
        pos[l] = size_t(floor(px));
      }
      vector<size_t> neis = hex->getNei(pos);
      for (int j = 0 ; j < neis.size(); j++)
      {
        size_t ind = neis[j];
        if (ind >= i) continue;

        float r2 = 0;
        for (int k =0; k < dim; k++)
          r2 += dists[k][i][ind]*dists[k][i][ind];
        
        dists[dim][i][ind] = r2;
        dists[dim+1][i][ind] = sqrtf(r2);
      }
      
    }
    
  }

  
  void setB(vector< vector<float> > &Bin)
  {
    for(int i = 0 ; i < dim ; i++)
    {
      bounds[i][0] = Bin[i][0];
      bounds[i][1] = Bin[i][1];
    }
  }
  
  
  void setX(vector<vector<float> > &in)
  {
    x.setCO(in);

    hex->resetMe();
//    cout << "hex reset done ...";
    vector<vector<size_t> > aps(size);
    #pragma omp parallel for
    for(size_t i = 0 ; i < size; i++)
    {
      aps[i].resize(dim);
      for (int k = 0 ; k < dim ; k++)
      {
        float px = hex->n*(x.co[k][i]-bounds[k][0])/(bounds[k][1]-bounds[k][0]);
        aps[i][k] = size_t(floor(px));
      }        
    }
//    cout << "hex wanna append"<<endl;
    hex->append(aps);

  }

  
  
  void setV(vector<vector<float> > &in)
  {
    v.setCO(in);
  }
  
  
  void setF(vector<vector<float> > &in)
  {
    F.setCO(in);
  }
  
  vector<float> getX(int i)
  {
	return x.getCO(i);
  }
  
  vector<float> getV(int i)
  {
	return v.getCO(i);
  }
  
  vector<float> getF(int i)
  {
	return F.getCO(i);
  }
  
  vector<vector<float> > getB()
  {
    vector<vector<float> > out;
    cout << "getB"<<endl;
    for(int i=0; i<dim ; i++)
    {
      vector<float> c(2);
      out.push_back(c);
      out[i][0] = bounds[i][0];
      out[i][1] = bounds[i][1];
      cout << out[i][0] << " " <<out[i][1]<<endl;
    }
    
    return out;
  }
  
  //debug stuff
  void foo_p(int c=0)
  {
    Space* h;
    
    switch(c)
    {
      case 0: h = &x;
        break;
      case 1: h = &v;
        break;
      case 2: h = &F;
        break;
    }
    
    cout<< h->name << endl;
    for (int k = 0 ; k < dim ; k++)
    {
    for(size_t i = 0 ; i < size_pre; i++)
    {
      cout << h->co[k][i] << " ";
    }
    cout << endl;
    }
    cout<<endl;
    
  }

  void correctV2(float mean, float q99)
  {
    #pragma omp parallel for
    for (int i = 0 ; i < size; i++)
    {
      float sum = 0;
      for (int k = 0 ; k < dim ; k++)
        sum += v.co[k][i]*v.co[k][i];
        
      float coef = 1;
      sum = sqrtf(sum);
      if (sum >= q99)
      {
        coef = mean/sum;
        for (int k = 0 ; k < dim ; k++)
          v.co[k][i] = v.co[k][i]*coef;
        //cout << "now the speed is bigger ( "<<sum<<" > "<<q99<<") settin "<<mean<<endl;
      }
    }
  }

  void inflate()
  {
    x.inflate();
    v.inflate();
    newv.inflate();
    F.inflate();
    
    size_pre *= 2;
    m.resize(size_pre);
    r.resize(size_pre);
    
    for (int j = 0 ; j < nodists; j++)
    {
      dists[j].resize(size_pre);
      for (int i = 0 ; i < size_pre; i++)
      {
        dists[j][i].resize(size_pre);//i);
      }
    }
    
    if (u_glue)
    {
      glue.resize(size_pre);
      for (size_t i = 0; i < size_pre; i++)
      {
        glue[i].resize(size_pre);
      }
    }
    glued.resize(size_pre);
  }

	//euler step with velocity correction in case of large velocity differences
  void integrate(float dt)
  {
//    cout << "integrating" << endl;
    float epsconst = 1e-10;
    
    #pragma omp parallel for
    for(size_t i = 0; i<size; i++ )
    {
      float oldNorm = 0;
      float newNorm = 0;
      for(int k = 0 ; k < dim ; k++)
      {     
        newv.co[k][i] = v.co[k][i] + ( F.co[k][i] / m[i] )*dt;
        oldNorm += v.co[k][i]*v.co[k][i];
        newNorm += newv.co[k][i]*newv.co[k][i];
      }
      
      float coef = 1;
      if ( newNorm > 5* oldNorm)
      {
        coef = sqrtf((1.5*oldNorm +epsconst) / newNorm);
        //cout << "BIG V JUMP"<<endl;
      } 
      for (int k = 0 ; k < dim; k++)
      {        
        x.co[k][i] += v.co[k][i]*dt;
        
        //snad lepsi pro v(t)
        v.co[k][i] = coef*newv.co[k][i];

      }
    }
  }

	// put particles in their places
  void enforceBounds()
  {
//    cout << "enforcing bounds" << endl;

    #pragma omp parallel for
    for(size_t i = 0; i<size; i++ )
    {
      for (int k = 0 ; k < dim; k++)
      {
        //PERIODIC
        //while (x.co[k][i] < bounds[k][0]) x.co[k][i] += bounds[k][1]-bounds[k][0];
        //while (x.co[k][i] >= bounds[k][1]) x.co[k][i] -= ( bounds[k][1]-bounds[k][0] );
        
        //RIGID
        if (x.co[k][i] < bounds[k][0])
          {
            if (v.co[k][i] < 0)
              v.co[k][i] *= -1;
            x.co[k][i] = bounds[k][0];
          }
        if (x.co[k][i] >= bounds[k][1])
          {
            if (v.co[k][i] > 0)
              v.co[k][i] *= -1;
            x.co[k][i] = bounds[k][1];
          }
        
      }
    }

    setX(x.co);
  }

  vector<float> getMinDists()
  {
    vector<float> out(size);
    
    #pragma omp parallel for
    for (size_t i = 0; i < size; i++)
    {
      float mind = 9e23;
      for (size_t j =0 ; j<size; j++)
      {
        if (i == j) continue;
        
        float d = 0;
        for (int k=0 ; k < dim; k++)
          d += powf(x.co[k][i] - x.co[k][j],2);
        d = sqrtf(d);
        if (d < mind)
          mind = d;
      }
      
      out[i] = mind;
    }
    
    return out;
  }

  float getEK()
  {
    float sum = 0;
    #pragma omp parallel for schedule(dynamic,2) reduction(+:sum)
    for (size_t i = 0; i < size; i++)
    {
      float s = 0;
      for (int k = 0 ; k < dim ; k++)
      {
        s+= v.co[k][i]*v.co[k][i];
      }
      sum += m[i]*s/2;
    }
    return sum;
  }
  
  void debug_print()
  {
    cout << "size: "<< size << " , E: " << Energy << endl;
    
    cout << "co / v / F /m / r" << endl;
    for(int i = 0 ; i  < 3; i++)
      foo_p(i);
    
    cout<<"m"<<endl;
    for(size_t i = 0 ; i < size; i++)
    {
      cout << m[i] << " ";
    }
    cout << endl;
    
    cout<<"r"<<endl;
    for(size_t i = 0 ; i < size; i++)
    {
      cout << r[i] << " ";
    }
    cout << endl;
    
  }
  
  void debug_printF()
  {
    cout << "size: "<< size_pre << " , E: " << Energy << endl;
    
    cout << "co / v / F /m / r" << endl;
    for(int i = 0 ; i  < 3; i++)
      foo_p(i);
    
    //~ cout<<"m"<<endl;
    //~ for(size_t i = 0 ; i < size_pre; i++)
    //~ {
      //~ cout << m[i] << " ";
    //~ }
    //~ cout << endl;
    
    //~ cout<<"r"<<endl;
    //~ for(size_t i = 0 ; i < size_pre; i++)
    //~ {
      //~ cout << r[i] << " ";
    //~ }
    //~ cout << endl;
    
  }
  
  ~Swarm()
  {
    m.clear();
    r.clear();
    delete hex;
    glued.clear();
  }

};
