#include "model.h"
#include "potencial.h"

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>


PYBIND11_MODULE(moleculator, m)
{
  m.doc()="For molecular simulation and animation";
	
  namespace py = pybind11;
  using namespace pybind11::literals;
  
  py::class_<Space>(m,"Space")
    .def(py::init<size_t, string>(),"size"_a=0, "name"_a="Foo")
    .def_readonly("co",&Space::co)
    .def("setCONST", &Space::setCONST)
    .def("setCO",&Space::setCO)
    ;
  
  py::class_<Swarm>(m,"Swarm")
    .def(py::init<size_t, size_t, bool, float>(),"size"_a=0, "bins"_a = 20, "glue"_a = false, "rlim"_a=27.4e-9)
    .def("setX",&Swarm::setX)
    .def("setV",&Swarm::setV)
    .def("setF",&Swarm::setF)
    .def("setB",&Swarm::setB)
    .def("getX",&Swarm::getX)
    .def("getB", &Swarm::getB)
    .def("getV",&Swarm::getV)
    .def("getF",&Swarm::getF)
    .def_readonly("E",&Swarm::Energy)
    .def_readwrite("m",&Swarm::m)
    .def_readwrite("r",&Swarm::r)
    .def_readwrite("size",&Swarm::size)
    .def_readwrite("size_pre",&Swarm::size_pre)
//    .def_readwrite("bounds",&Swarm::bounds)
    .def("debug_print",&Swarm::debug_print)
    .def("debug_printF",&Swarm::debug_printF)
    .def("calcDists",&Swarm::calcDists)
    .def("integrate", &Swarm::integrate)
    .def("getEK", &Swarm::getEK)
    .def("addParticle", &Swarm::addParticle)
    .def("inflate", &Swarm::inflate)
    .def("enforceBounds", &Swarm::enforceBounds)
    .def("correctV2", &Swarm::correctV2)
    .def("getMinDists", &Swarm::getMinDists)
    //.def_readwrite("glued",&Swarm::glued)
    .def("getGlued", &Swarm::getGlued)
    ;
    
  m.def("force_Newton",&calcNewton)
    .def("force_LJ",&calcLJ)
    .def("force_Hadamar",&calcHadamar)
    .def("force_HadamarGlue",&calcHadamarGlue)
    .def("force_Brown",&calcBrown)
    .def("potencial_LJ", &calcLJE)
    ;
    
}
