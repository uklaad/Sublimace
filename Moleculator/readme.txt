Molecular dynamics based on newtonian physics. Simulates interacting system of many particles.
New particles are added into the simulation at random times (acc. to physics).
SW contains several bugs, which were further unattended - they should be visible in the example output (bellow).

Build with cmake into some other folder. (cd *buildfolder* ; cmake *pathToMolSource* ; make )
Build will copy python libs, with an example usage in "buildFolder/MolLib.py", to the build dir.


Core computations written in c++ (include folder), wraped into python via PyBind11 (cppwrap/moleculator.cpp).
Python side of SW is in folder "pylibs" in source folder.

Saving, loading and defining a Simulation is done via "Simulation.py". Here forces, which are to be applied are selected with others simulation parameters. It mainly refers to wrapped C++ function.
Constants are set in "Constants.py" file.
Running of a simulation is taken care of in "Animate.py", which is dedicated to create evolution videos.

