import numpy as np
import matplotlib.pyplot as plt

import scipy.stats as st
from matplotlib.colors import LogNorm

from fitweib import *
import os


##konstanty

k_bolt = 1.38e-23   #energie castic vody
Temp = 233
collisions_per_sec = 3.4e14     #ovlivni casovou osu

#hmotnosti castic
mNC = 1.56e-20
mH2O = 2.99e-26





#barvy grafu
colors = ['k', 'b', 'r', 'g', 'm', 'olive', 'maroon', 'darkcyan', 'lightsalmon', 'aqua', 'yellow', 'sienna','purple','dimgray']

def ensure_dir(folder):
    #dir = os.path.dirname(__file__)
    file_path = os.getcwd()+'/'+folder+'/'
    print(file_path)
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)

class myweib:
  def __init__(self, beta, theta):
    self.beta = beta
    self.theta = theta
  def pdf(self, x):
    c = self.beta/self.theta
    return c*np.power(x/self.theta,self.beta-1)*np.exp(-np.power( x/self.theta ,self.beta) )

#simulace jedne castice
class Sim:
  def __init__(self, random = False):
    self.muT = 1.
    self.mNC = mNC
    self.mH2O = mH2O
    self.T =Temp
    
    self.v = 0
    self.alfa = 0
    self.alfaplot = 0
    self.random = random
    
    self.x = [0,0]
    self.dt = 1./collisions_per_sec
    self.r = 0
    
    self.vdir = [0,0]
    
  def setMe(self, x, v):
    self.v = v
    self.r = np.sqrt((x[0]**2 + x[1]**2))
    self.x[0] = x[0]
    self.x[1] = x[1]
    
    #self.alfa = np.random.rand()*2*np.pi
    if x[0] == 0:
      self.alfa = np.pi/2
      if x[1] < 0:
        self.alfa *= 1
    else:
      self.alfa = np.arctan(x[1]/x[0])
      if x[0] < 0:
        self.alfa += np.pi
    
  def collide(self):
    alfa = np.random.rand()*np.pi
    
    a = np.sqrt(k_bolt*self.T/self.mH2O)
    V2h2o = st.maxwell.rvs(0,a)
    
    
    Vh2o = V2h2o  #np.sqrt(st.maxwell.rvs(0,a))
    if not self.random:
      Vh2o = 524  #np.sqrt(st.maxwell.rvs(0,a))
    
    VC = self.v
    
    vdir = [  np.cos(alfa)*Vh2o - VC , np.sin(alfa)*Vh2o]
    
    
    alfa = np.arctan(vdir[1]/vdir[0])
    if alfa < 0:
      alfa += np.pi 
      
    self.alfaplot = alfa
    
    
    #print(vdir)
    #print(alfa)
    
    VH = np.sqrt( vdir[0]**2 + vdir[1]**2 )
    
    VC = 0
    
    PI = self.mNC*VC - self.mH2O*VH
    EI = self.mNC*(VC**2) + self.mH2O*(VH**2)
    
    m1 = self.mNC
    m2 = self.mH2O
    
    x0 = PI/m1
    di = (PI**2)/(m1**2) -(1./m1 + 1./m2)*((PI**2)/m1 - EI)
    jm = (1.+m2/m1)
    x = (x0 + np.sqrt(  di  ) )/jm
    x2 = (x0 - np.sqrt(  di  ) )/jm ####miss
    
    #print('{0}:{1},{2}'.format(VH,x,x2))
    
    v1 =  -(PI - m2*x)/m1
    #print(v1)
    
    #self.v += v1**2 + np.cos(alfa)*VC*v1
    
    sign = np.random.rand()
    if sign<0.5:
      sign = -1.
    else:
      sign = 1.
      
    #pseudodir
    v1*=np.random.rand()
      
    VV0 = self.v + v1*np.cos(alfa)
    VV1 = sign*v1*np.sin(alfa)
    
    
    nalfa = np.arctan(VV1/VV0)
    if VV0 < 0:
      nalfa += np.pi
      
    self.alfa += nalfa
    
    self.v = np.sqrt(VV0**2 + VV1**2)

    self.vdir[0] = self.v*np.cos(self.alfa)
    self.vdir[1] = self.v*np.sin(self.alfa)

    self.x[0] += self.vdir[0]*self.dt
    self.x[1] += self.vdir[1]*self.dt

    self.r = np.sqrt( self.x[0]**2 + self.x[1]**2 )
 
#vicero simulaci pro statisticke analyzy
class SimSwarm:
  def __init__(self, N, random = True, saveAll = False):
    self.n = N
    self.pop = []
    
    self.saveAll = saveAll
    
    for i in range(N):
      self.pop.append( Sim(random) )
      
    self.v = [ np.zeros(N) ]
    self.r = [ np.zeros(N) ]
    self.x = [ np.zeros((N,2)) ]
    
    self.allv = [np.zeros(N)]
    self.allr = [np.zeros(N)]
    self.allx = [np.zeros((N,2)) ]
    self.ts = [0]
    
    self.t = 0
    self.dt = 1./1e11
    
    self.vsigma = []
    self.vsigmaT = []
    
  def saveMe(self,fname):
    """slouzi pro ukladani simulace pro moznost na ni navazat"""
    T = len(self.ts)
    with open('sims/'+fname+'.txt','w') as f:
      f.write('{0},{1},{2}\n'.format(self.n,T,self.dt))
      for t in range(T):
        f.write('{0}\n'.format(self.ts[t]))
        
        txt = ''
        for i in range(self.n):
          txt += '{0},'.format(self.r[t][i])
        txt += '\n'
        f.write(txt)
        
        txt = ''
        for i in range(self.n):
          txt += '{0},'.format(self.v[t][i])
        txt += '\n'
        f.write(txt)
        
        txt = ''
        for i in range(self.n):
          txt += '{0},'.format(self.x[t][i,0])
        txt += '\n'
        f.write(txt)
        
        txt = ''
        for i in range(self.n):
          txt += '{0},'.format(self.x[t][i,1])
        txt += '\n'
        f.write(txt)
        
        
  def collide(self,step = 1):
    """spocita 'step' kolizi pro vsechny castice simulace"""
    newv = np.zeros(self.n)
    newr = np.zeros(self.n)
    newx = np.zeros((self.n,2))
    
    if self.saveAll:
      newallv = np.zeros((step,self.n))
      newallr = np.zeros((step,self.n))
      newallx = np.zeros((step,self.n,2))
    
    vsig = np.zeros(self.n)
    for i in range(self.n):
      for s in range(step):
        self.pop[i].collide()
        
        #pocita se rozptyl v X souradnici
        vsig[i] = self.pop[i].vdir[0]
        if self.saveAll:
          newallv[s,i] = self.pop[i].v
          newallr[s,i] = self.pop[i].r
          newallx[s,i,:] = self.pop[i].x
        
        
        
      newv[i] = self.pop[i].v
      newr[i] = self.pop[i].r
      newx[i] = self.pop[i].x
    
    
    self.v.append(newv)
    self.r.append(newr)
    self.x.append(newx)
    
   
   #pokud chceme drzet v pameti celou historii stavu
    if self.saveAll:
      for s in range(step):
        self.allv.append(newallv[s])
        self.allr.append(newallr[s])
        self.allx.append(newallx[s])
      
    self.t += self.dt*step
    self.ts.append(self.ts[-1]+1*step)

    self.vsigma.append( vsig.var() )
    self.vsigmaT.append( vsig.var()/self.t )


  def showW(self, fromID = 1, noplot=15, noplot2 = 1, folder = 'imgs', tronly = False):
    ensure_dir(folder)
    if not tronly:  #tronly - vykresli jen trajektorie
      
      fromID = min(fromID, len(self.v))
      
      diffig = plt.figure()
      diffigG = plt.figure()
      difhandles = []
      difhandlesG = []
      difnames = []
      difnamesG = []
      
      Gxmin = 1.
      Gxmax = 0.
      
      difys = []
      difts = []
      difvars = []
      
      for i in range(fromID, len(self.v)):
        time = self.ts[i]
        
        fig = diffig
        plt.figure(fig.number)
        N = 50
        x = self.r[i]
        xmax = x.max()
        t = np.linspace(0,xmax,N+1)
        step2 = (t[1]-t[0])/2
        tt = np.linspace(step2, xmax-step2,N)
        y = np.zeros(N)
        for m in range(N):
          bli = -1
          uli = t[m+1]
          if i > 0:
            bli = t[m]
          mask = (x> bli) & (x<=uli)
          y[m] = len(x[mask]) / (2*(uli-t[m])) #(np.pi*uli - np.pi*t[m])#(np.pi*(uli**2) - np.pi*(t[m]**2)) 
          
        y/=len(x)
        
        if tt[0] < Gxmin:
          Gxmin = tt[0]
        if tt[-1] > Gxmax:
          Gxmax = tt[-1]
        
        sigy = sum(y*np.power(tt,2)) / sum(y)
        
        print("var:\t{0}\t{1}".format( sigy , time*self.dt))
        
        p, = plt.plot(tt,y)
        difhandles.append(p)
        nam = 'time: {0}col, {1:.2E}sec'.format(time, round(time*self.dt,1))
        difnames.append(nam)
        plt.title('time (collisions) {0}, sec: {1:0.2}'.format(time, time*self.dt))
        fig.savefig(folder+'/RSnorm_PART{0}.png'.format(time) )  
        
        
        coll = (np.random.rand(),np.random.rand(),np.random.rand())
        
        plt.figure(diffigG.number)
        ax = plt.gca()
        p = ax.scatter(tt,y, color = coll)
        sd = np.sqrt(sigy)
        
        
        #gn = 2*st.norm.pdf(tt,0,sd)
        theta, beta = fitweib( x )
        dist = st.dweibull(beta, 0 , theta  )
        gn = dist.pdf(tt)
        pg, = plt.plot(tt, gn, color = coll)
        
        #~ plt.figure()
        #~ plt.plot(tt,gn)
        #~ plt.show(0)
        #~ 
        #~ print(tt)
        #~ print(gn)
        
        difhandlesG.extend([p,pg])
        difnamesG.extend([nam,nam+'GAUS'])
        
        difys.append(y)
        difts.append(time*self.dt)
        difvars.append(sd)

    
    plt.figure(diffig.number)
    ax = plt.gca()
    ax.legend(difhandles, difnames)
    plt.title('Radialni rozdeleni castic')
    diffig.savefig(folder+'/vars.png')
    
    
    Gxdif = Gxmax - Gxmin
    Gxmean = (Gxmax + Gxmin) / 2
    Gxmin = Gxmean - Gxdif/1.9
    Gxmax = Gxmean + Gxdif/1.9
    
    plt.figure(diffigG.number)
    ax = plt.gca()
    #ax.legend(difhandlesG, difnamesG)
    ax.set_xlim([Gxmin, Gxmax])
    plt.title('Odhad radialniho rozdeleni (Weibull) mereni')    
    diffigG.savefig(folder+'/varsEst.png')
    
    
    with open(folder+'/ys.txt','w') as f:
      f.write('{0}\n'.format(len(difts)))
      for i in range(len(difts)):
        f.write('{0}\t{1}'.format(difts[i],difvars[i]))
        for j in range(len(difys[i])):
          f.write('\t{0}'.format(difys[i][j]))
        f.write('\n')
    
    count = 0
    for k in range(noplot2):
      fig = plt.figure()
      for i in range(count,noplot+count):
        x = np.zeros((len(self.v),2))
        for j in range(len(self.v)):
          x[j] = self.x[j][i,:]
        plt.plot(x[:,0],x[:,1])
        plt.scatter(x[:,0],x[:,1])
      plt.title('time (collisions) up to {0}, sec: {1:0.2}'.format(self.ts[-1], self.ts[-1]*self.dt))
      fig.savefig(folder+'/trajectories{0}.png'.format(k))  
      count += noplot


    self.printXV(folder+'/TRAJ')



  def printXV(self, fname):
    namex = fname+'_x.csv'
    xs = self.x[-1]
    
    with open(namex,'w') as f:
      for i in range(len( xs )):
        f.write('{0},{1}\n'.format(xs[i,0],xs[i,1]))
  
    namev = fname+'_x.csv'
    vs = self.v[-1]
    
    with open(namev,'w') as f:
      for i in range(len( xs )):
        f.write('{0}\n'.format(vs[i]))
       
  def showSigma(self):
    fig = plt.figure()
    plt.title('Vyvoj rozptylu na ose X')
    plt.plot(self.vsigma)
    plt.show(0)
    fig = plt.figure()
    plt.title('Rozptyl X / cas simulace')
    plt.plot(self.vsigmaT)
    plt.show(0)
        
  

  def show(self, fromID = 1, noplot=15, noplot2 = 1, folder = 'imgs', tronly = False, normed = True):
    """tronly = vykresli jen 'noplot2' ruznych vizualizaci trajektorii od ulozeneho mezikroku 'fromID' pokazde pro 'noplot' ruznych castic castic do slozky 'folder'"""
    ensure_dir(folder)
    
    if not tronly:
      #zbytek vykreslovani
      fromID = min(fromID, len(self.v))
      
      diffig = plt.figure()
      diffigG = plt.figure()
      difhandles = []
      difhandlesG = []
      difnames = []
      difnamesG = []
      
      Gxmin = 1.
      Gxmax = 0.
      
      difys = []
      difts = []
      difvars = []
      
      for i in range(fromID, len(self.v)):
        time = self.ts[i]
        
        fig= plt.figure()
        plt.hist(self.v[i], bins = 50, normed = normed)
        plt.title('Distribution of particle Quad velocity; time (collisions) {0}, sec: {1:0.2}'.format(time, time*self.dt))
        fig.savefig(folder+'/V_t{0}.png'.format(time) )

        plt.close()

        fig= plt.figure()
        plt.hist(self.x[i][:,0], bins = 50 , normed = normed)
        plt.title('Distribution of particles over X0; time (collisions) {0}, sec: {1:0.2}'.format(time, time*self.dt))
        fig.savefig(folder+'/X0_t{0}.png'.format(time) )

        plt.close()

        fig= plt.figure()
        plt.hist(self.x[i][:,1], bins = 50, normed = normed)
        plt.title('Distribution of particles over X1; time (collisions) {0}, sec: {1:0.2}'.format(time, time*self.dt))
        fig.savefig(folder+'/X1_t{0}.png'.format(time) )

        plt.close()
        
        fig= plt.figure()
        plt.hist(self.r[i], bins = 50, normed = normed)
        plt.title('Radial distribution of particles; time (collisions) {0}, sec: {1:0.2}'.format(time, time*self.dt))
        fig.savefig(folder+'/R_t{0}.png'.format(time) )
  
        plt.close()
      
        fig = plt.figure()
        plt.hist2d(self.x[i][:,0],self.x[i][:,1], norm=LogNorm(), bins = 50)
        #plt.hexbin(self.x[i][:,0],self.x[i][:,1], norm=LogNorm(), bins = 50)
        plt.title('2D particle position histogram; time (collisions) {0}, sec: {1:0.2}'.format(time, time*self.dt))
        fig.savefig(folder+'/DH_t{0}.png'.format(time) )

        plt.close()


        #vykreslovani odhadu hustoty pravdepodobnosti v zavislosti na vzdalenosti r
        fig = diffig
        plt.figure(fig.number)
        N = 50
        x = self.r[i]
        xmax = x.max()
        t = np.linspace(0,xmax,N+1)
        step2 = (t[1]-t[0])/2
        tt = np.linspace(step2, xmax-step2,N)
        y = np.zeros(N)
        for m in range(N):
          bli = -1
          uli = t[m+1]
          if i > 0:
            bli = t[m]
          mask = (x> bli) & (x<=uli)
          y[m] = len(x[mask]) / (4*np.pi*(uli**2-t[m]**2))
          
        y/=sum(y)*step2*2
        
        if tt[0] < Gxmin:
          Gxmin = tt[0]
        if tt[-1] > Gxmax:
          Gxmax = tt[-1]
        
        
        sigy = sum(y*np.power(tt,2)) / sum(y)
        
        print("var:\t{0}\t{1}".format( sigy , time*self.dt))
        p, = plt.plot(tt,y)
        difhandles.append(p)
        nam = 'time: {0}col, {1:.2E}sec'.format(time, round(time*self.dt,1))
        difnames.append(nam)
        plt.title('Radial distribution; time (collisions) {0}, sec: {1:0.2}'.format(time, time*self.dt))
        fig.savefig(folder+'/RSnorm_PART{0}.png'.format(time) )  
        
        
        coll = (np.random.rand(),np.random.rand(),np.random.rand())
        
        plt.figure(diffigG.number)
        ax = plt.gca()
        p = ax.scatter(tt,y, color = coll)
        sd = np.sqrt(sigy)
        
        
        gn = 2*st.norm.pdf(tt,0,sd)
        pg, = plt.plot(tt, gn, color = coll)
        
        #~ print(tt)
        #~ print(gn)
        
        difhandlesG.extend([p,pg])
        difnamesG.extend([nam,nam+'GAUS'])
        
        difys.append(y)
        difts.append(time*self.dt)
        difvars.append(sd)

    
      plt.figure(diffig.number)
      ax = plt.gca()
      ax.legend(difhandles, difnames)
      plt.title('Neparametricky odhad radialniho rozdeleni castic')  
      diffig.savefig(folder+'/vars.png')
    
    
      Gxdif = Gxmax - Gxmin
      Gxmean = (Gxmax + Gxmin) / 2
      Gxmin = Gxmean - Gxdif/1.9
      Gxmax = Gxmean + Gxdif/1.9
      
      plt.figure(diffigG.number)
      ax = plt.gca()
      #ax.legend(difhandlesG, difnamesG)
      ax.set_xlim([Gxmin, Gxmax])
      plt.title('Odhad radialniho rozdeleni castic (Gauss rozdeleni) + mereni')  
      diffigG.savefig(folder+'/varsEst.png')
      
      
      with open(folder+'/ys.txt','w') as f:
        f.write('{0}\n'.format(len(difts)))
        for i in range(len(difts)):
          f.write('{0}\t{1}'.format(difts[i],difvars[i]))
          for j in range(len(difys[i])):
            f.write('\t{0}'.format(difys[i][j]))
          f.write('\n')
      
    count = 0
    for k in range(noplot2):
      
      xmin = 0
      xmax = 0
      
      scale = 1e9
      
      fig = plt.figure()
      for i in range(count,noplot+count):
        coll = (np.random.rand(),np.random.rand(),np.random.rand())
        x = np.zeros((len(self.v),2))
        for j in range(len(self.v)):
          x[j] = self.x[j][i,:]
          for kk in range(2):
            if x[j][kk] < xmin:
              xmin = x[j][kk]
            if x[j][kk] > xmax:
              xmax = x[j][kk]
        
        plt.plot(scale*x[:,0],scale*x[:,1], color = coll)
        plt.scatter(scale*x[:,0],scale*x[:,1], color = coll)
      plt.title('Trajectories up to {1:0.1E} [s] (={0:0.1E} collisions)'.format(self.ts[-1], self.ts[-1]*self.dt))
      
      #plt.axis('scaled')
      xmin *= scale*1.05
      xmax *= scale*1.05
      
      xxmax = max(xmax, -xmin)
      
      plt.xlim(-xxmax,xxmax)
      plt.ylim(-xxmax,xxmax)
      plt.xlabel('X [1E-9 m]')      
      plt.ylabel('Y [1E-9 m]')      
      
      fig.savefig(folder+'/trajectories{0}.tiff'.format(k), format='tiff')  
      plt.close()
      count += noplot


    self.printXV(folder+'/TRAJ')



  def showSigmaEvolution(self, plots = 0, folder = 'foo', showgaus = True):
    ensure_dir(folder)
    x = self.x
    Nx = len(x)
    
    n = self.n
    
    time = np.zeros(Nx)
    sig = np.zeros(Nx)
    
    wmode = np.zeros(Nx)
    
    xmin = 0
    xmax = 0
    
    
    for i in range(Nx):
      t = np.zeros(2*n)
      t[:n] = x[i][:,0]
      t[n:] = x[i][:,1]
      
      time[i] = self.ts[i]
      sig[i] = np.sqrt( sum(np.power(t,2))/(2*n) )

      xxmin = t.min()
      xxmax = t.max()
      
      if xxmin < xmin:
        xmin = xxmin
      if xxmax > xmax:
        xmax = xxmax
      
      v = self.r[i] 
      theta, beta = fitweib( v )  
      wmode[i] = theta * np.sqrt(1./2)
    
    
    #gn = st.weibull_min.pdf(xx/theta,2)
    
    fig = plt.figure()
    
    scalex = 10**6
    scaley = 10**10
    
    #p1 = plt.scatter(scalex* time*self.dt, scaley*sig)
    p1, = plt.plot(scalex* time*self.dt, scaley*sig)
    p2 = plt.scatter(scalex* time*self.dt,scaley*wmode, color = 'r')
    #p2, = plt.plot(scalex* time*self.dt,scaley*wmode)
    plt.title('Estimation of the evolution of standard deviation')
    plt.legend([p1,p2],['Standard deviation (from Gauss model)','mode of Weibull distribution'],bbox_to_anchor=(0.7, 0.95))
    
    plt.xlabel('Time [1E-6 s]')
    plt.ylabel('Distance from origin [1E-10 m]')
    
    plt.xlim([0, max(scalex*time*self.dt) ])
    plt.ylim([0, max(scaley*sig)])
    
    #fig.savefig(folder+'/sigmaEvo.png')
    fig.savefig(folder+'/sigmaEvo.tiff',format='tiff')
    plt.close()
#    plt.show(0)
    
    vs = np.zeros(Nx)
    with open(folder+'/sds.txt','w') as f:
      for i in range(1,Nx):
        if i > 0:
          vs[i] = (sig[i] - sig[i-1]) / (time[i]-time[i-1])
        f.write('t(col):{0}\tt(s):{1}\tsd:{2}\tvsd:{3}\n'.format(time[i],time[i]*self.dt,sig[i],vs[i]))
      vmean = vs.mean()
      f.write('mean vsd:{0}\n'.format(vmean))

    fig = plt.figure()
    
    scalex = 10**6
    scaley = 10**15
    
    plt.plot(scalex*time*self.dt,scaley*vs, color = 'k')
    
    plt.xlabel('Time [1E-6 s]')
    plt.ylabel('dsd/dt [1E-15 m/s]')
    
    plt.title('Estimation of standard deviation time derivative')
    
    #plt.xlim([0,max(scalex*time*self.dt)])
    #plt.ylim([0,max(scaley*vs)])
    
    #fig.savefig(folder+'/sigmaDiffEvo.png')
    fig.savefig(folder+'/sigmaDiffEvo.tiff',format='tiff')
    plt.close()
#    plt.show(0)
    
    if showgaus:
      fig = plt.figure()
      ax = plt.gca()
      xx = np.linspace(xmin,xmax,250)
      difhandles = []
      difnames = []
      
      ddd = int(Nx/plots)
      cnt = Nx-1
      for j in range(plots):
        i = Nx-1 - j*ddd
        #coll = (np.random.rand(),np.random.rand(),np.random.rand())
        coll = colors[j]
        
        gn = st.norm.pdf(xx, 0, sig[i])
        p, = plt.plot(xx,gn, color = coll)
        difhandles.append(p)
        nam = 'time: {0}col, {1:.2E}sec'.format(self.ts[i], self.ts[i]*self.dt,1)
        difnames.append(nam)
        
        infl = [-sig[i],sig[i]]
        ax.scatter(infl,st.norm.pdf(infl, 0, sig[i]), color = coll)
        
        
        
      plt.title('Evolution of particle radial position distribution')
      plt.legend(difhandles, difnames)
      #plt.show(0)
      #fig.savefig(folder+'/gausovky.png')
      fig.savefig(folder+'/gausovky.tiff',format='tiff')
      plt.close()


#nacteni simulace ze souboru (pro rozkouskovani vypoctu)
def loadSim(fname):
  """nacte simulaci ze souboru, ktery je ve slozce 'sims'"""
  ss = None
  
  with open('sims/'+fname+'.txt','r') as f:
    cnt = 0
    cmod = 0
    for line in f:
      line= line.strip('\n')
      ls = line.split(',')
      
#      print(ls)
      
      if cnt == 0:
        ss = SimSwarm(int(ls[0]))
        ss.dt = float(ls[2])
        cnt +=1
      
      else:
        if cmod%5 == 0:
          ss.ts.append( int(ls[0]) )
        else:
          vec = np.zeros(ss.n)
          for i in range(ss.n):
            vec[i] = float(ls[i])
          
          if cmod%5 == 1:
            ss.r.append(vec)
          elif cmod%5 == 2:
            ss.v.append(vec)
          elif cmod%5 == 3:
            xx = np.zeros((ss.n,2))
            xx[:,0] = vec
            ss.x.append(xx)
          else:
            ss.x[-1][:,1] = vec
        cmod +=1
  
  for i in range(len(ss.pop)):
    ss.pop[i].setMe([ ss.x[-1][i,0], ss.x[-1][i,1]] , ss.v[-1][i])
  
  return ss
  
  
ensure_dir('sims')
  
if __name__ == '__main__':
  #ss=  loadSim('L3.5-s1000-19-15000')
   
   
  #run the simulation
  ss = SimSwarm(250)  #pocet castic
  for i in range(10):   #pocet obrazku a bodu trajektorie
    print(i)
    ss.collide(150)     #pocet srazek mezi dvema casovymi kroky na obrazku
    ss.saveMe('simName_{0}'.format(i+1))  #ulozit pro mozne navazani



  #check results    
  ss.showSigma()
  ss.show(1,noplot=15, noplot2 = 3, folder = 'itest')   #vykresli trajektorie, 2d histogramy a odhady radialniho rozdeleni via Gauss
  ss.showW(2,noplot=10, noplot2 = 3, folder = 'Wtest')    #vykresli trajektorie a odhady rad. rozdeleni via Weibull
  ss.showSigmaEvolution(5,'foo')    #vykresli vyvoj rozptylu polohy v case
      

