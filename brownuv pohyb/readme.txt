in-source parameters :
boltzmann_k
Temp(erature)
collisions_per_sec
masses of NanoParticle and water molecule.

(Class Sim)
Simulates random collisions of a nanoparticle with water molecules. Collision effects are derived from the conservation of energy and momentum. (Class Sim)
Large number of collisions is simulated and respective time is estimated calculated from mean time to collision (t = no_col*ttc).

(Class SimSwarm)
Simulation is carried on for large number of individual independent nanoparticles and the results are assessed statisticaly.
Whole swarm can be saved in and loaded from a file.

(SimSwarm.show*)
Program outputs trajectories of single particles; evolution of the particle position distribution (2d, 1d - x,r); plot of evolution of particle position variance and its estimated derivative.


Ran on __main__, the program will run a simple simulation and outputs plots into separate files.

