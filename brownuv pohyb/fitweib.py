import numpy as np
import scipy.stats as sts
import scipy.optimize as sco

def fitweib(ts):
  
  ts = np.array(ts)
  lts = np.log(ts)
  
  slts = sum(lts)
  n = len(ts)
  
  def f(x):
    b = x[0]
    
    tb = np.power(ts,b)
    a1 = sum(tb)/n
    a2 = sum(tb*lts)
    
    out = a1 - a2/(n/b+slts)
    return out**2

  x0 = [2.]
  res = sco.minimize(f, x0, method='bfgs')
  
  beta = res.x[0]
  beta = 2.
  theta = np.power( sum(np.power(ts,beta))/n , 1./beta)
  
  print("fitweib> b: {0}, t:{1}".format(beta,theta))
  
  return theta, beta
